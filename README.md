# Capstone_Drone_Research

Pacific University Capstone Project by Josh Napoles and Ryan Maroney
Joan the Drone: An Autonomous Tool for Wildlife Research

Here is a link to the google doc for our final report with the user guide
https://docs.google.com/document/d/1R49GwgganVgSqMXIOcrasOqgG_kDZ_muFnPujXp6_9A/edit?usp=sharing

Here is the link to the google sheets for our flight log
https://docs.google.com/spreadsheets/d/1XjiO7Pjo5FdMrqwdMclswoI9OgHXdf9VfT5ySXrrd6w/edit?usp=sharing

https://github.com/emlid/Navio2 Emlid Navio2 Repository (Sensor Test Programs)

https://github.com/emlid/ardupilot  Ardupilot Repository

https://github.com/emlid/navio2-docs Navio2 Setup installation instructions

http://ardupilot.org/copter/index.html arducopter documentation 

https://docs.emlid.com/navio2/ Navio2 Docs

http://ardupilot.org/dev/index.html ArduPilot Documentation for Developers

http://marte.aslab.upm.es/redmine/files/dmsf/p_drone-testbed/170324115730_268_Quigley_-_Programming_Robots_with_ROS.pdf Programming Robots with ROS book

http://ardupilot.org/planner2/index.html APM Planner 2 Documentation