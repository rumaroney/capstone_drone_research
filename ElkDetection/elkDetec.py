######################################################
# FILE: elkDetec.py
# AUTHOR: Josh Napoles
# PURPOSE: detects elk in thermal video and
#          diplays the number of detected elk to
#          the terminal
#
#
######################################################
import os
import numpy as np
import cv2 as cv2
import time

maskThresh = 220 # threshold value between 0-255
num_Frames = 0   # frame counter

cap = cv2.VideoCapture('Hagg_Lake_Deer.mp4')

# set up output windows
cv2.namedWindow('frame', cv2.WINDOW_NORMAL)
cv2.namedWindow('mask', cv2.WINDOW_NORMAL)
cv2.resizeWindow('frame', 500, 500)
cv2.resizeWindow('mask', 200, 200)

kernel = np.ones((5, 5), np.uint8)

rectangle_list = list ()


while(cap.isOpened()):
    ret, frame = cap.read()

    # only looking at every second frame for performance
    if (num_Frames % 1 == 0):
      rectangle_list.clear()

      frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
      #frame_inverted = cv2.bitwise_not(frame_gray)
      ret, mask = cv2.threshold (frame_gray, maskThresh, 255, cv2.THRESH_BINARY)

      # removes false positives from background (white)
      erosion = cv2.erode(mask, kernel, iterations=1)
      opening = cv2.morphologyEx (erosion, cv2.MORPH_OPEN, kernel)


      dilation2 = cv2.dilate(opening, kernel, iterations=2)
      dilation = cv2.dilate(mask, kernel, iterations=3)
      contours, _ = cv2.findContours (dilation, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

      for contour in contours:
        x, y, w, h = cv2.boundingRect (contour)
        #remove large noise
        if w * h < 2000: 
          rectangle_list.append((x, y, w, h))
          cv2.rectangle(frame, (x, y, w+20, h+20), (0, 0, 255), 2)

      # Display number of bounding rectangles
    os.system('clear')
    print("ELK ON SCREEN: ", len(rectangle_list))

    cv2.imshow('frame',frame)
    cv2.imshow('mask',dilation)
    if (num_Frames == 1):
      time.sleep(5)


    num_Frames += 1
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
